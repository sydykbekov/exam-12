const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Photo = require('../models/Photo');

const auth = require('../middleware/auth');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
    router.get('/', (req, res) => {
        Photo.find().populate('user')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, upload.single('image')], (req, res) => {
        const photoData = req.body;
        photoData.user = req.user._id;

        if (req.file) {
            photoData.image = req.file.filename;
        } else {
            photoData.image = null;
        }

        const photo = new Photo(photoData);

        photo.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;

        Photo.find({user: id}).populate('user')
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', auth, (req, res) => {
        Photo.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;