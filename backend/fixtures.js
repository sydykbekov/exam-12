const mongoose = require('mongoose');
const config = require('./config');

const Photo = require('./models/Photo');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('photos');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [Alex, John] = await User.create({
        username: 'Alex',
        password: '123'
    }, {
        username: 'John',
        password: '123'
    });

    await Photo.create({
        title: 'Рысь',
        image: 'Cat.jpg',
        user: Alex._id
    }, {
        title: 'Красивая собака',
        image: 'Dog.jpg',
        user: Alex._id
    }, {
        title: 'Природа',
        image: 'Mountain.jpg',
        user: John._id
    }, {
        title: 'Париж',
        image: 'Paris.jpg',
        user: John._id
    });

    db.close();
});