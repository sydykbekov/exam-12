import axios from '../../axios-api';
import {FETCH_AUTHOR_PHOTOS, FETCH_PHOTOS_SUCCESS} from "./actionTypes";
import {push} from "react-router-redux";
import {NotificationManager} from 'react-notifications';

const fetchPhotosSuccess = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos};
};
const fetchAuthorPhotos = photos => {
    return {type: FETCH_AUTHOR_PHOTOS, photos};
};

export const fetchPhotos = () => {
    return dispatch => {
        axios.get('/photos').then(response => {
            dispatch(fetchPhotosSuccess(response.data));
        });
    }
};

export const fetchByAuthor = id => {
    return dispatch => {
        axios.get(`/photos/${id}`).then(response => {
            dispatch(fetchAuthorPhotos(response.data));
        })
    }
};

export const addPhoto = formData => {
    return dispatch => {
        axios.post('photos', formData).then(() => {
            NotificationManager.success('Фото успешно загружено!', 'Успех!');
            dispatch(push('/'));
        })
    }
};

export const removePhoto = id => {
    return dispatch => {
        axios.delete(`/photos/${id}`).then(() => {
            NotificationManager.success('Фото успешно удалено!', 'Успех!');
            dispatch(push(`/`));
        })
    };
};