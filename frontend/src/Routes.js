import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import NewPhoto from "./containers/NewPhoto/NewPhoto";
import Register from "./containers/Register/Register";
import Photos from "./containers/Photos/Photos";
import Login from "./containers/Login/Login";
import UserPhotos from "./containers/UserPhotos/UserPhotos";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login" />
);

const Routes = ({user}) => (
  <Switch>
    <Route path="/" exact component={Photos}/>
    <ProtectedRoute
      isAllowed={user}
      path="/new-photo"
      exact
      component={NewPhoto}
    />
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
    <Route path="/:id" exact component={UserPhotos}/>
  </Switch>
);

export default Routes;