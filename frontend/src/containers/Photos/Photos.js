import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Grid, PageHeader, Row, Modal, Thumbnail} from "react-bootstrap";
import {fetchPhotos} from "../../store/actions/photos";
import PhotoListItem from "../../components/PhotoListItem/PhotoListItem";

class Photos extends Component {
    state = {
        show: false,
        image: ''
    };

    photoToModal = image => {
        this.setState({image: image, show: true})
    };

    componentDidMount() {
        this.props.fetchPhotos();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Photos
                </PageHeader>
                <Grid>
                    <Row>
                        {this.props.photos.map(product => (
                            <PhotoListItem
                                key={product._id}
                                id={product.user._id}
                                title={product.title}
                                user={product.user.username}
                                image={product.image}
                                photoToModal={() => this.photoToModal(product.image)}
                            />
                        ))}
                    </Row>
                </Grid>
                <Modal show={this.state.show} bsSize="large">
                    <Modal.Body>
                        <Thumbnail src={'http://localhost:8000/uploads/' + this.state.image}/>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button bsStyle="warning" onClick={() => this.setState({show: false})}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    photos: state.photos.photos
});

const mapDispatchToProps = dispatch => ({
    fetchPhotos: () => dispatch(fetchPhotos())
});

export default connect(mapStateToProps, mapDispatchToProps)(Photos);