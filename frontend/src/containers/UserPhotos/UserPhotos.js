import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchByAuthor, removePhoto} from "../../store/actions/photos";
import {Button, Grid, Modal, PageHeader, Row, Thumbnail} from "react-bootstrap";
import PhotoListItem from "../../components/PhotoListItem/PhotoListItem";
import {Link} from "react-router-dom";

class UserPhotos extends Component {
    state = {
        show: false,
        image: ''
    };

    photoToModal = image => {
        this.setState({image: image, show: true})
    };

    componentDidMount() {
        this.props.fetchPhotosByAuthor(this.props.match.params.id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.match.params.id) this.props.fetchPhotosByAuthor(this.props.match.params.id);
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    {this.props.photos.length > 0 ? `${this.props.photos[0].user.username}'s gallery` : "Your gallery is empty!"}
                    {this.props.user && this.props.user._id === this.props.match.params.id &&
                    <Link to="/new-photo">
                        <Button bsStyle="primary" className="pull-right">
                            Add photo
                        </Button>
                    </Link>
                    }
                </PageHeader>
                <Grid>
                    <Row>
                        {this.props.photos && this.props.photos.map(product => (
                            <PhotoListItem
                                key={product._id}
                                id={product.user._id}
                                title={product.title}
                                user={product.user.username}
                                image={product.image}
                                photoToModal={() => this.photoToModal(product.image)}
                                remove={(this.props.user && this.props.user._id === product.user._id) ? () => this.props.remove(product._id) : null}
                            />
                        ))}
                    </Row>
                    <Modal show={this.state.show} bsSize="large">
                        <Modal.Body>
                            <Thumbnail src={'http://localhost:8000/uploads/' + this.state.image}/>
                        </Modal.Body>

                        <Modal.Footer>
                            <Button onClick={() => this.setState({show: false})}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </Grid>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    photos: state.photos.photos,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchPhotosByAuthor: id => dispatch(fetchByAuthor(id)),
    remove: id => dispatch(removePhoto(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPhotos)