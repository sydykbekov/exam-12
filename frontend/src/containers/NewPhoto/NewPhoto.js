import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {addPhoto} from "../../store/actions/photos";

class NewPhoto extends Component {
    render() {
        return (
            <Fragment>
                <PageHeader>New photo</PageHeader>
                <PhotoForm user={this.props.user._id} onSubmit={(formData) => this.props.addPhoto(formData)} />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    addPhoto: formData => dispatch(addPhoto(formData))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPhoto);