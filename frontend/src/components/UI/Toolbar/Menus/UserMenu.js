import React from 'react';
import {Nav, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserMenu = ({user, logout}) => {
    const navTitle = (
        <LinkContainer to={`/${user._id}`}>
            <NavItem>
                Hello, <b>{user.username}</b>!
            </NavItem>
        </LinkContainer>
    );

    return (
        <Nav pullRight>
            {user && navTitle}
            <NavItem onClick={logout}>Logout</NavItem>
        </Nav>
    )
};

export default UserMenu;