import React from 'react';
import {Button, Col, Thumbnail} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const PhotoListItem = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    return (
        <Col xs={6} md={3}>
            <Thumbnail src={image} style={{cursor: 'pointer'}} onClick={props.photoToModal}/>
            <h3>{props.title}</h3>
            <b>By: </b><Link to={'/' + props.id}>{props.user}</Link>
            {props.remove && <Button className="pull-right" onClick={props.remove} bsStyle="danger">Delete</Button>}
        </Col>
    );
};

PhotoListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    image: PropTypes.string
};

export default PhotoListItem;